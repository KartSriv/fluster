#!/bin/python

# from subprocess import Popen, PIPE
from cryptography.fernet import Fernet
import sys
import pyperclip

file = open(sys.argv[1]+'.key', 'rb')
key = file.read() # The key will be type bytes
file.close()

input_file = sys.argv[1]

with open(input_file, 'rb') as f:
    data = f.read()

fernet = Fernet(key)
encrypted = fernet.decrypt(data)

pyperclip.copy(encrypted)
pyperclip.paste()
