#!/bin/bash

if [ -z "$var" ]
then
      echo "Starting Session at "$1
      jupyter notebook $1
else
      jupyter notebook /home/kartsriv.JupyterNotebook/
fi

