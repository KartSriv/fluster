[![](http://i.imgur.com/LUJj8tt.png)](https://github.com/KartSriv/fluster)

<h1 align="center">

  <br>
</h1>
  </a>
  <a href="https://saythanks.io/to/KartSriv">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg"> <a href="https://app.codacy.com/app/KartSriv/fluster?utm_source=github.com&utm_medium=referral&utm_content=KartSriv/fluster&utm_campaign=Badge_Grade_Dashboard">
      <img src="https://api.codacy.com/project/badge/Grade/f2fb2b0d7edf46f7ade06b2d21937804">
  </a>
</p>

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)

**rickroll.sh** is taken from <a href="https://github.com/keroserene/rickrollrc">**keroserene/rickrollrc**</a>
