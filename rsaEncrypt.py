#!/bin/python

from cryptography.fernet import Fernet
import sys

key = Fernet.generate_key()
file = open(sys.argv[2]+'.key', 'wb')
file.write(key) # The key is type bytes still
file.close()
input_file = sys.argv[1]
output_file = sys.argv[2]
key = Fernet.generate_key()
file = open(sys.argv[2]+'.key', 'wb')
file.write(key) # The key is type bytes still
file.close()

data = input_file

fernet = Fernet(key)
encrypted = fernet.encrypt(data)

with open(output_file, 'wb') as f:
    f.write(encrypted)
